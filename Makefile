all : public_core.pdf

.PHONY : all clean

public_core.pdf : public_core.tex 
	pdflatex $^
	pdflatex $^

clean :
	rm -f *.eps *.pdf *.dat *.log *.out *.aux *.dvi *.ps *.nav *.snm *.toc *.vrb *~
